package org.angeledugo.springcloud.msvc.product.services;

import org.angeledugo.springcloud.msvc.product.entity.Product;

import java.util.List;
import java.util.Optional;

public interface ProductService {

    List<Product> listar();
    Optional<Product> porId(Long id);
    Product guardar(Product product);
    void eliminar(Long id);
}
