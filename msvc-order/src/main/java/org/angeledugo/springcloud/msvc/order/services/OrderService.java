package org.angeledugo.springcloud.msvc.order.services;

import org.angeledugo.springcloud.msvc.order.entity.Order;

import java.util.List;

public interface OrderService {

    List<Order> getOrdersByUserId(Long userId);
    Order saveOrder(Order order);
    void deleteOrder(Long id);
}
