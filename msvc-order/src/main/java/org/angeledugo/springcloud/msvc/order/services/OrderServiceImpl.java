package org.angeledugo.springcloud.msvc.order.services;

import org.angeledugo.springcloud.msvc.order.entity.Order;
import org.angeledugo.springcloud.msvc.order.respositories.OrderRespository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrderServiceImpl implements OrderService{

    @Autowired
    OrderRespository respository;


    @Override
    public List<Order> getOrdersByUserId(Long userId) {
        return respository.findByUserId(userId);
    }

    @Override
    public Order saveOrder(Order order) {
        return respository.save(order);
    }

    @Override
    public void deleteOrder(Long id) {
        respository.deleteById(id);
    }
}
