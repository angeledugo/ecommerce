package org.angeledugo.springcloud.msvc.order.respositories;

import org.angeledugo.springcloud.msvc.order.entity.Order;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface OrderRespository extends CrudRepository<Order, Long> {
    List<Order> findByUserId(Long userId);

}
