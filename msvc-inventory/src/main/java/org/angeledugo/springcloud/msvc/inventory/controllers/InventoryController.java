package org.angeledugo.springcloud.msvc.inventory.controllers;

import org.angeledugo.springcloud.msvc.inventory.entity.Inventory;
import org.angeledugo.springcloud.msvc.inventory.services.InventoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class InventoryController {

    @Autowired
    private InventoryService service;

    @GetMapping("/{productId}")
    public ResponseEntity<Inventory> getInventoryByProductId(@PathVariable Long productId) {
        return service.getInventoryByProductId(productId)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }

    @PostMapping
    public Inventory createInventory(@RequestBody Inventory inventory) {
        return service.saveInventory(inventory);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Inventory> updateInventory(@RequestBody Inventory inventoryDetails, @PathVariable Long id) {
        return service.getInventoryByProductId(id)
                .map(inventory -> {
                    inventory.setQuantity(inventoryDetails.getQuantity());
                    Inventory updateInventoy = service.saveInventory(inventory);
                    return ResponseEntity.ok(updateInventoy);
                })
                .orElse(ResponseEntity.notFound().build());
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteInventory(@PathVariable Long id) {
        service.deleteInventory(id);
        return ResponseEntity.noContent().build();
    }


}
