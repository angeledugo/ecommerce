package org.angeledugo.springcloud.msvc.inventory.services;

import org.angeledugo.springcloud.msvc.inventory.entity.Inventory;
import org.angeledugo.springcloud.msvc.inventory.repositories.InventoryRespository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class InventoryServiceImpl implements InventoryService{
    @Autowired
    private InventoryRespository respository;

    @Override
    public Optional<Inventory> getInventoryByProductId(Long productId) {
        return respository.findByProductId(productId);
    }

    @Override
    public Inventory saveInventory(Inventory inventory) {
        return respository.save(inventory);
    }

    @Override
    public void deleteInventory(Long id) {
        respository.deleteById(id);
    }
}
