package org.angeledugo.springcloud.msvc.inventory.repositories;

import org.angeledugo.springcloud.msvc.inventory.entity.Inventory;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface InventoryRespository  extends CrudRepository<Inventory, Long> {
    Optional<Inventory> findByProductId(Long productId);
}
