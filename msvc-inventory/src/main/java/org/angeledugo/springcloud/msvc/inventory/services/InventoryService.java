package org.angeledugo.springcloud.msvc.inventory.services;

import org.angeledugo.springcloud.msvc.inventory.entity.Inventory;

import java.util.Optional;

public interface InventoryService {
    Optional<Inventory> getInventoryByProductId(Long productId);
    Inventory saveInventory(Inventory inventory);
    void deleteInventory(Long id);


}
